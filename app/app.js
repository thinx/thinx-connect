const logger = require("pino")();
const https = require("https");
const http = require("http");
const mdns = require("mdns-js");
const httpProxy = require("http-proxy");
const execSync = require("child_process").execSync || require("sync-exec");
const rootCas = require("ssl-root-cas").create();

require("colors"); // used for log
require("https").globalAgent.options.ca = rootCas;
require("ssl-root-cas").inject();

const wsPort = 7444;
var enable_mqtt = true; // todo, change to configurable per env...

if (enable_mqtt) {
  logger.info("Starting MQTT listener on port 1883...");

  /* MQTT HTTPS <*/
  httpProxy.createProxyServer({
    target: "https://thinx.cloud:1883",
    agent: http.globalAgent,
    headers: {
      host: "thinx.cloud"
    }
  }).listen(1883);

  logger.info("Starting MQTTS listener on port 8883...");
  /* MQTTHTTP */
  httpProxy.createProxyServer({
    target: "https://thinx.cloud:8883",
    agent: https.globalAgent,
    headers: {
      host: "thinx.cloud"
    }
  }).listen(8883);
}

logger.info("Starting HTTP->HTTPS listener on port 7442->7443...");

/* HTTP to HTTPS proxy */
httpProxy.createProxyServer({
  target: "https://thinx.cloud:7443",
  agent: https.globalAgent,
  secure: false,
  headers: {
    host: "thinx.cloud"
  }
}).listen(process.env.HTTP_PROXY_IN || 5000); // process.env.PROXY_IN should be 7442

logger.info("Starting HTTPS listener o port 7443...");

/* HTTPS to HTTPS fall-trough */
httpProxy.createProxyServer({
  target: "https://thinx.cloud:7443",
  agent: https.globalAgent,
  headers: {
    host: "thinx.cloud"
  }
}).listen(7443);

logger.info("Starting WSS Proxy on port " + wsPort);

/* proxy websockets */
httpProxy.createProxyServer({
    target: "wss://thinx.cloud:" + wsPort,
    secure: false,
    ws: true
}).listen(wsPort);

var package_info = require("./package.json");
var name = package_info.name;
var version = package_info.version;
var CMD = "git rev-list HEAD --count";
var temp = execSync(CMD).toString().replace("\n", "");
logger.info("CMD result: " + temp);
// var version = String(parseInt(temp));

logger.info("");
logger.info("-=[".red + " ☢ " + name.white + " v".red.bold + version.red.bold +
  " rev. ".red + version.red +
  " ☢ " + " ]=-".red);

logger.info("should advertise a thinx service on port 7442");
var service = mdns.createAdvertisement(mdns.tcp("thinx"), 7442, {
  name:"proxy",
  txt:{
    txtvers:"1"
  }
});
service.start();

/*eslint no-undef: "off"*/

process.stdin.resume();

// stop on Ctrl-C
process.on("SIGINT", function () {
  service.stop();
  // give deregistration a little time
  setTimeout(function onTimeout() {
    /*eslint no-process-exit: "off"*/
    process.exit();
  }, 1000);
});
