FROM node:chakracore-10.13.0
ARG VERSION=master

RUN apt-get update \
  && apt-get install --no-install-recommends -y git bash nano wget libavahi-compat-libdnssd-dev \
  && alt-get clean && rm -rf /var/lib/apt/lists/*

RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN echo "127.0.0.1 thinx" >> /etc/hosts

COPY . /home/node

WORKDIR /home/node/app

RUN npm config set strict-ssl false \
 && npm install . --only-prod

# USER node ; should be by default

# for testing only (GitLab AutoDevOps)
EXPOSE 5000

# Reserved for WS
# EXPOSE 7444

# HTTP & HTTPS
EXPOSE 7442
EXPOSE 7443

# MQTT & MQTTS
EXPOSE 1883
EXPOSE 8883

CMD [ "node", "app.js" ]
